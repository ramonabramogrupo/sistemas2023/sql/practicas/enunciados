![ramon](imgs/ramon.jpeg)

<hr>

![sql](/imgs/todosql.jpg)

![logo](imgs/rpeque.png)Hola 🖐 soy Ramon Abramo 

<table class="js-file-title">
<thead>
<tr>
<th>

INDICE

</th>
</tr>
</thead>
<tbody>
<tr>
<td>

Este es el índice del repositorio. Navega por todo el contenido desde aquí. Aquí encontrarás el mapa de contenidos. Explora todo lo que tenemos para ofrecerte desde esta página.

</td>
</tr>
<tr>
<td>

[[_TOC_]]

</td>
</tr>
</tbody>
</table>


<br>

> **Estos son las enunciados de cada una de las practicas de la unidad de SQL** <br>
> Ademas te coloco un SQL con el guion inicial en cada una de las practicas. De esta forma solamente teneis que abrir el guion y complementarle para realizar cada una de las practicas

<br>

![soluciones](imgs/ENUNCIADOS.png)

# Practicas de LDD
- practica 1 .- Implementar Coches y Clientes
- practica 2 .- Implementar Coches y Clientes
- practica 3 .- Implementar Coches, clientes y poblacion
- practica 4 .- Implementar Empleado, supervisa, dependiente, proyectos, departamentos
- practica 5 .- Implementar Alumnos, Delegados, modulos, profesores
- practica 6 .- Implementar Cliente, Coche, Compra, Revision. Ejercicios ALTER TABLE
- practica 7 .- Implementar Alumno, Realiza, Practica, FechaP
- practica 8 .- Implementar Cliente, compra, producto, proveedor. Otro Camioneros, paquetes

# Practicas de LMD

- practica 9 .- Consultas de seleccion de una sola tabla (ciclistas)
- practica 9A .- Consultas de seleccion de un sola tabla (empresas). Orientada con funciones y LIKE
- practica 10 .- Consultas de totales (ciclistas)
- practica 11 .- Consultas de seleccion de una sola tabla y de totales (ciclistas). Repaso
- practica 12 .- Consultas de combinacion internas (ciclistas)
- practica 13 .-  Consultas de seleccion repaso (empiezo a utilizar clausulas conjuntos) (emple,depart)
- **[Practica14.-](PRACTICA14/)** Consultas de seleccion de una sola tabla (tienda)

![sql](imgs/sql.jpg)

<br>


<table class="js-file-title">
<thead>
<tr>
<th>

INDICE

</th>
</tr>
</thead>
<tbody>
<tr>
<td>

Este es el índice del repositorio. Navega por todo el contenido desde aquí. Aquí encontrarás el mapa de contenidos. Explora todo lo que tenemos para ofrecerte desde esta página.

</td>
</tr>
<tr>
<td>

[[_TOC_]]

</td>
</tr>
</tbody>
</table>


<br>