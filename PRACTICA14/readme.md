![ramon](../imgs/profe.png)
<hr>
![portada](../imgs/1.jpg)
![portada](../imgs/2.jpg)

<br>


<table class="js-file-title">
<thead>
<tr>
<th>

INDICE

</th>
</tr>
</thead>
<tbody>
<tr>
<td>

Este es el índice del repositorio. Navega por todo el contenido desde aquí. Aquí encontrarás el mapa de contenidos. Explora todo lo que tenemos para ofrecerte desde esta página.

</td>
</tr>
<tr>
<td>

[[_TOC_]]

</td>
</tr>
</tbody>
</table>


<br>

# Practica 14

## Consultas de seleccion de una tabla

En esta práctica, nos sumergiremos en el corazón de SQL: las consultas de selección. SQL, o Lenguaje de Consulta Estructurado, es el estándar de facto para interactuar con bases de datos relacionales. A través de una serie de ejercicios prácticos, exploraremos cómo la cláusula SELECT nos permite acceder a los datos almacenados en una tabla, seleccionando específicamente los registros y campos que necesitamos.

Comenzaremos con consultas simples, seleccionando todas las columnas de una tabla con SELECT * FROM nombre_tabla. Gradualmente, introduciremos cláusulas adicionales como WHERE para filtrar registros, ORDER BY para ordenar los resultados, GROUP BY para agrupar datos similares y HAVING para filtrar grupos. También veremos cómo las funciones de agregación como COUNT, SUM, MAX, y MIN pueden proporcionar estadísticas resumidas de nuestros datos.

Cada cláusula y función se explicará con ejemplos detallados, asegurando que al final de esta práctica, tengas una comprensión sólida de cómo realizar consultas de selección efectivas y eficientes en una sola tabla.

Prepárate para convertirte en un maestro de las consultas de selección y descubrir el poder de SQL para manipular y analizar datos.

## Base de datos tienda
La base de datos con la que vamos a trabajar contiene dos tablas de Fabricantes y Productos.

### Esquema de relaciones

La base de datos con la que vamos a trabajar dispone del siguiente esquema de relaciones.

![esquema](../imgs/esquema_relaciones.png)

### Datos

El contenido de las tablas es el siguiente.

**Fabricante**

|codigo|nombre|
|------|------|
|1|Asus|
|2|Lenovo|
|3|Hewlett-Packard|
|4|Samsung|
|5|Seagate|
|6|Crucial|
|7|Gigabyte|
|8|Huawei|
|9|Xiaomi

**Producto**

|codigo|nombre|precio|codigo_fabricante|
|------|------|------|-----------------|
|1|Disco duro SATA3 1TB|86.99|5|
|2|Memoria RAM DDR4 8GB|120|6|
|3|Disco SSD 1 TB|150.99|4|
|4|GeForce GTX 1050Ti|185|7|
|5|GeForce GTX 1080 Xtreme|755|6|
|6|Monitor 24 LED Full HD|202|1|
|7|Monitor 27 LED Full HD|245.99|1|
|8|Portátil Yoga 520|559|2|
|9|Portátil Ideapd 320|444|2|
|10|Impresora HP Deskjet 3720|59.99|3|
|11|Impresora HP Laserjet Pro M26nw|180|3

### Codigo SQL

Os coloco el codigo SQL para crear la base de datos tienda que utilizamos.

La teneis disponible en el propio repositorio.

~~~sql
SET NAMES 'utf8';

DROP DATABASE IF EXISTS tienda;

CREATE DATABASE IF NOT EXISTS tienda
CHARACTER SET utf8mb4
COLLATE utf8mb4_0900_ai_ci;

--
-- Set default database
--
USE tienda;

--
-- Create table `fabricante`
--
CREATE TABLE IF NOT EXISTS fabricante (
  codigo int UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
ROW_FORMAT = DYNAMIC;

--
-- Create table `producto`
--
CREATE TABLE IF NOT EXISTS producto (
  codigo int UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  precio double NOT NULL,
  codigo_fabricante int UNSIGNED NOT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB,
AUTO_INCREMENT = 14,
AVG_ROW_LENGTH = 1489,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
ROW_FORMAT = DYNAMIC;

--
-- Create foreign key
--
ALTER TABLE producto
ADD CONSTRAINT producto_ibfk_1 FOREIGN KEY (codigo_fabricante)
REFERENCES fabricante (codigo);

-- 
-- Dumping data for table fabricante
--
INSERT INTO fabricante VALUES
(1, 'Asus'),
(2, 'Lenovo'),
(3, 'Hewlett-Packard'),
(4, 'Samsung'),
(5, 'Seagate'),
(6, 'Crucial'),
(7, 'Gigabyte'),
(8, 'Huawei'),
(9, 'Xiaomi');

-- 
-- Dumping data for table producto
--
INSERT INTO producto VALUES
(1, 'Disco duro SATA3 1TB', 86.99, 5),
(2, 'Memoria RAM DDR4 8GB', 120, 6),
(3, 'Disco SSD 1 TB', 150.99, 4),
(4, 'GeForce GTX 1050Ti', 185, 7),
(5, 'GeForce GTX 1080 Xtreme', 755, 6),
(6, 'Monitor 24 LED Full HD', 202, 1),
(7, 'Monitor 27 LED Full HD', 245.99, 1),
(8, 'Portátil Yoga 520', 559, 2),
(9, 'Portátil Ideapd 320', 444, 2),
(10, 'Impresora HP Deskjet 3720', 59.99, 3),
(11, 'Impresora HP Laserjet Pro M26nw', 180, 3);
~~~

## Enunciados

![enunciados](../imgs/ENUNCIADOS.png)

- Ejercicio 1.- Lista el nombre de todos los productos que hay en la tabla producto.
- Ejercicio 2.- Lista los nombres y los precios de todos los productos de la tabla producto.
- Ejercicio 3 .- Muestra los productos ordenados por nombre de forma ascendente y despues por precio de forma descendente.
- Ejercicio 4 .- Lista el nombre de los productos y el precio y un campo nuevo que nos muestre el codigo del producto seguido de un guion y despues el nombre del producto (1-Disco Duro). Este nuevo campo denominarlo Referencia
- Ejercicio 5.- Lista el nombre de los productos, el precio y el precio sin decimales. Para quitar los decimales quiero truncar. Utiliza los siguientes alias para las columnas: nombre de producto, precio, precio truncado.
- Ejercicio 6.- Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a mayúscula.
- Ejercicio 7.- Lista el nombre de todos los fabricantes y en otra columna obtenga en mayúsculas los dos primeros caracteres del nombre del fabricante.
  - opcion 1 : utilizando SUBSTRING
  - opcion 2 : utilizando SUBSTRING_INDEX
  - opcion 3 : utilizando LEFT
- Ejercicio 8.- Lista los nombres y los precios de todos los productos de la tabla producto, redondeando el valor del precio a 1 decimal.



## Ayudas a las consultas


![ayudas](../imgs/AYUDA.jpg)

- Ejercicio 7

| SUBSTRING_INDEX |
| --- | 
|La función SUBSTRING_INDEX en MySQL se utiliza para devolver una subcadena de una cadena antes de un número especificado de ocurrencias de un delimitador. |
|La sintaxis básica es la siguiente: <br><br>`SUBSTRING_INDEX(cadena, delimitador, número)` <br><br>| 
|Donde:<br> - cadena es la cadena de la que quieres extraer la subcadena. <br>- delimitador es el carácter o caracteres que separan las subcadenas dentro de la cadena.<br>- número es el número de veces que ocurre el delimitador en la cadena antes de que se extraiga la subcadena.|

| LEFT |
| --- | 
|La función LEFT en MySQL se utiliza para extraer un número específico de caracteres desde el inicio (izquierda) de una cadena.| 
|La sintaxis básica es la siguiente:<br><br> `LEFT(cadena, número)` <br><br> |
|Donde:<ul><li>cadena es la cadena de la que quieres extraer caracteres.</li><li>número es el número de caracteres que quieres extraer desde el inicio de la cadena.</li><ul>|

<table>
<thead>
<tr>
<th> Funcion Round</th>
</tr>
</thead>
<tbody>
<tr>
<td>
La función ROUND en MySQL se utiliza para redondear un número a un número específico de decimales.

La sintaxis básica es la siguiente: 

```sql
ROUND(numero, decimales) 
```
Donde:

- número es el número que quieres redondear.
- decimales es el número de decimales a los que quieres redondear el número.

</td>
</tr>
<tr>
<td>
Veamos un ejemplo:

```sql
SELECT ROUND(123.456, 2);
```

En este caso, la función ROUND devolverá el número 123.46, ya que 123.456 redondeado a 2 decimales es 123.46.

</td>
</tr>
</tbody>
</table>


<br>


<table class="js-file-title">
<thead>
<tr>
<th>

INDICE

</th>
</tr>
</thead>
<tbody>
<tr>
<td>

Este es el índice del repositorio. Navega por todo el contenido desde aquí. Aquí encontrarás el mapa de contenidos. Explora todo lo que tenemos para ofrecerte desde esta página.

</td>
</tr>
<tr>
<td>

[[_TOC_]]

</td>
</tr>
</tbody>
</table>


<br>