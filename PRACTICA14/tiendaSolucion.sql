﻿USE tienda;

# Ejercicio 1.- Lista el nombre de todos los productos que hay en la tabla producto.


# Ejercicio 2.- Lista los nombres y los precios de todos los productos de la tabla producto.

# Ejercicio 3 .- Muestra los productos ordenados por nombre de forma ascendente y despues por precio de forma descendente.


# Ejercicio 4 .- Lista el nombre de los productos y el precio y un campo nuevo que nos muestre el codigo del producto seguido de un guion y despues el nombre del producto (1-Disco Duro). Este nuevo campo denominarlo Referencia


# Ejercicio 5.- Lista el nombre de los productos, el precio y el precio sin decimales. Para quitar los decimales quiero truncar. Utiliza los siguientes alias para las columnas: nombre de producto, precio, precio truncado.

# Ejercicio 6.- Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a mayúscula.


# Ejercicio 7.- Lista el nombre de todos los fabricantes y en otra columna obtenga en mayúsculas los dos primeros caracteres del nombre del fabricante.

-- opcion 1
-- utilizando SUBSTRING

-- opcion 2
-- utilizando SUBSTRING_INDEX

-- opcion 3
-- utilizando LEFT


# Ejercicio 8.- Lista los nombres y los precios de todos los productos de la tabla producto, redondeando el valor del precio a 1 decimal.

# Ejercicio 9.- Lista los nombres y los precios de los productos de la tabla producto cuyo precio se encuentre entre 500 y 700 .

-- opcion 1 
-- utilizando between 

-- opcion 2
-- utilizando and

  -- opcion 3
  -- utilizando el operador de conjuntos intersect 

# Ejercicio 10.- Lista el codigo del fabricante de los fabricantes que tienen productos en la tabla producto.


# Ejercicio 11.- Lista el codigo de los fabricantes que tienen productos en la tabla producto cuyos precios son menores de 100 € o mayores de 400€

-- opcion 1
-- Realizarlo con OR


-- opcion 2 
-- Realizarlo con el operador de conjuntos UNION

# Ejercicio 12.- Devuelve una lista con las 5 primeras filas de la tabla producto ordenados por precio de forma descendente


# Ejercicio 13.-Devuelve una lista con 2 filas a partir de la cuarta fila de la tabla fabricante ordenados por codigo de forma ascendente. Es decir quiero la cuarta y la quinta fila.


# Ejercicio 14.- Lista el nombre y el precio del producto más barato. (Utilice solamente las cláusulas ORDER BY y LIMIT)


# Ejercicio 15.- Lista el nombre y el precio del producto más caro. (Utilice solamente las cláusulas ORDER BY y LIMIT)


# Ejercicio 16.- Lista el nombre de los productos que tienen un precio mayor o igual a 400€.


# Ejercicio 17.-Lista el nombre de los productos que no tienen un precio mayor o igual a 400€.

-- opcion 1
-- sin utilizar NOT


-- opcion 2
-- utilizando NOT


# Ejercicio 18.- Lista todos los productos que no tengan un precio entre 80€ y 300€ (no me valen los que valgan 80 o 300)

-- opcion 1
-- sin utilizar NOT

-- opcion 2
-- utilizando NOT


# Ejercicio 19.- Lista todos los productos que tengan un precio mayor que 200€ y que el identificador de fabricante sea igual a 6.


# Ejercicio 20.- Lista todos los productos donde su el codigo del fabricante sea 2, 3 o 6. 

-- opcion 1
-- sin utilizar el operador IN


-- opcion 2
-- utilizando el operador IN


-- opcion 3
-- utilizando el operador de conjuntos UNION

# Ejercicio 21.- Lista los nombres de los fabricantes cuyo nombre empiece por la letra A o por la letra S.

-- opcion 1
-- utilizando comodines


-- opcion 2
-- utilizando SUBSTRING

-- opcion 3
-- utilizando LEFT


# Ejercicio 22.- Lista los nombres de los fabricantes cuyo nombre contenga el carácter X.

-- opcion 1
-- utilizando comodines

-- opcion 2
-- utilizando LOCATE

-- opcion 3
-- utilizando SUBSTRING_INDEX

# Ejercicio 23.- Lista los nombres de los productos de los cuales no conocemos el precio

# Ejercicio 24.- Listar el nombre de todos los productos junto con un campo calculado nuevo denominado Tipo. Este campo tipo debe tener el valor BARATO si el precio del producto es menor que 200 y CARO en caso contrario

# Ejercicio 25.- Listar el nombre de todos los productos junto con un campo calculado nuevo denominado Tipo. Este campo tipo debe tener el valor BARATO si el precio del producto es menor que 200 y NORMAL si el precio esta entre 200 y 400 y CARO en caso de ser mayor a 400

# Ejercicio 26.- Listar el nombre de los productos y los precios. El precio de los productos debe estar formateado de tal forma que salgan 5 digitos en la parte entera y dos digitos en la parte DECIMAL
